<?php

  define('HOST','localhost');
  define('USER','root');
  define('PASS','');
  define('DB','lazday_crud');
  $db_connect = mysqli_connect(HOST,USER,PASS,DB) or die('Unable to Connect');

  header('Content-Type: application/json');
  date_default_timezone_set('Asia/Jakarta');

  function response($status_code, $json) {
    if ($status_code == 200) header("HTTP/1.1 200 OK");
    else header("HTTP/1.1 400 Bad Request");
    echo json_encode( $json );
  }

?>